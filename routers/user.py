from fastapi import APIRouter
from fastapi import Path,Query,Request,Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel,Field
from typing import List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from schemas.user import User

user_router = APIRouter()


@user_router.post('/login',tags=['auth'])
def login(user:User):
  if user.email=="admin@gmail.com" and user.password=='admin':
    token:str = create_token(user.dict())
  return JSONResponse(status_code = 200,content=token) 